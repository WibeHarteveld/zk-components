(function () {
  const styleEl = document.querySelector('link[href*="klantdomein.min.css"]');
  const styleSwitcherEl = document.querySelector('.js-sg-style-select');

  function getFileName(url) {
    return url.split(/(\\|\/)/g).pop();
  }

  function getThemeName(url) {
    const fileName = getFileName(url);

    return fileName.split('-')[0];
  }

  function switchTheme(newThemeName) {
    const fileName = getFileName(styleEl.href);
    const newStyleHref = styleEl.href.split(fileName)[0] + newThemeName + '-' + fileName.split('-')[1];

    sessionStorage.setItem('themeName', newThemeName);
    styleEl.href = newStyleHref;
    styleSwitcherEl.value = newThemeName;
  }

  const themeName = getThemeName(styleEl.href);

  if (sessionStorage.getItem('themeName')) {
    const savedCurThemeName = sessionStorage.getItem('themeName');

    switchTheme(savedCurThemeName);
  } else {
    styleSwitcherEl.value = themeName;
  }

  styleSwitcherEl.addEventListener('change', event => {
    switchTheme(event.target.value);
  }, false);

}());
