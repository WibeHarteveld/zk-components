(function ($) {
  const $document = $(document);

  const tooltipTemplate =
    `<div class="c-tooltip tooltip" role="tooltip">
      <div class="c-tooltip__arrow tooltip-arrow"></div>
      <div class="c-tooltip__inner tooltip-inner"></div>
    </div>`;

  const tooltipTemplateWhite =
    `<div class="c-tooltip c-tooltip--white tooltip" role="tooltip">
      <div class="c-tooltip__arrow tooltip-arrow"></div>
      <div class="c-tooltip__inner tooltip-inner"></div>
    </div>`;

  // Initialize tooltips
  // 1. Enable dynamic HTML content to have tooltips added.
  $document.tooltip({
    selector: '.js-tooltip', // [1]
    template: tooltipTemplate,
  });

  // White variant
  $('.js-tooltip-white').tooltip({
    template: tooltipTemplateWhite,
  });
}(jQuery));
