import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import withDocs from 'storybook-readme/with-docs';
import Alert from '@zilverenkruis/alert';
import readme from '@zilverenkruis/alert/README.md';

import '@zilverenkruis/base/zk-base.scss';
import '@zilverenkruis/alert/c-alert.scss';

const stories = storiesOf('Alert', module);

stories.addDecorator(withKnobs);
// stories.addDecorator(withDocs(readme));

stories.add('with text', () => (
  <div>
  <Alert onDismiss={action('closed')} closeLabel={text('Close label', 'Sluit')}>
    <strong>Heads up!</strong> {text('Text', 'This is an alert')}
  </Alert>
  <Alert onDismiss={action('closed')} closeLabel={text('Close label', 'Sluit')}>
    <strong>Heads up!</strong> {text('Text', 'This is an alert')}
  </Alert>
  </div>
));

stories.add('with some emoji', () => (
  <Alert onDismiss={action('closed')}>
    <span role="img" aria-label="so cool">😀 😎 👍 💯</span>
    <strong>Holy guacamole!</strong> Best check yo self, you're not looking too good.
  </Alert>
));
