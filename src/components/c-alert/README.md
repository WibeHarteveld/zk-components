# Alert
Provide contextual feedback messages for typical user actions with the handful of available and flexible alert messages.

## Design & API Documentation
[zk's design guidelines: Alert](/design/components/alerts)

## Installation
```sh
npm install @zilverenkruis/button
```

## Basic Usage
### HTML Structure

<button class="mdc-button">
  Button
</button>

```js
<Alert bsStyle="warning" closeLabel="Sluiten" onDismiss>
  <strong>Holy guacamole!</strong> Best check yo self, you're not looking too
  good.
</Alert>
```

### Styles

```scss
@import "@zilverenkruis/button"
```

## Variants

## Style Customization
### CSS Classes
CSS Class | Description
--- | ---
`c-alert` | Mandatory. Defaults to an info alert.
`c-alert--success` | Optional. Styles ...
`c-alert--success-2` | Optional. Styles ...
`c-alert--info` | Optional. Styles ...
`c-alert--info-2` | Optional. Styles ...
`c-alert--warning` | Optional. Styles ...
`c-alert--warning-2` | Optional. Styles ...
`c-alert--danger` | Optional. Styles ...
`c-alert--danger-2` | Optional. Styles ...
`c-alert--no-radius` | Optional. Styles ...
`c-alert--inside-top` | Optional. Styles ...
`c-alert--inside-bottom` | Optional. Styles ...
`c-alert--small` | Optional. Styles ...
`c-alert--large` | Optional. Styles ...

### Sass Mixins

To customize a button's color and properties, you can use the following mixins.

#### Basic Sass Mixins

MDC Button uses [MDC Theme](../mdc-theme)'s `primary` color by default. Use the following mixins to customize it.

Mixin | Description
--- | ---
`mdc-button-filled-accessible($container-fill-color)` | Sets the container fill color for a contained (_raised_ or _unelevated_) button, and updates the button's ink, icon, and ripple colors to meet accessibility standards

#### Advanced Sass Mixins

These mixins will override the color of the container, ink, outline or ripple. It is up to you to ensure your button meets accessibility standards.

Mixin | Description
--- | ---
`mdc-button-container-fill-color($color)` | Sets the container fill color to the given color.
`mdc-button-icon-color($color)` | Sets the icon color to the given color.
`mdc-button-ink-color($color)` | Sets the ink color to the given color, and sets the icon color to the given color unless `mdc-button-icon-color` is also used.
`mdc-button-corner-radius($corner-radius)` | Sets the corner radius to the given number (defaults to 2px).
`mdc-button-horizontal-padding($padding)` | Sets horizontal padding to the given number.
`mdc-button-outline-color($color)` | Sets the outline color to the given color.
`mdc-button-outline-width($width, $padding)` | Sets the outline width to the given number (defaults to 2px) and adjusts padding accordingly. `$padding` is only required in cases where `mdc-button-horizontal-padding` is also included with a custom value.

##### Caveat: Edge and CSS Custom Properties

text...
