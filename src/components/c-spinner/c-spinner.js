// Loading spinner

;(function($, window, document, undefined) {
  'use strict';

  var showSpinner = function (id, $target, position) {
    if ($('#' + id).length !== 0) { return; }

    if (!$target) {
      $target = $('body');
    }

    if (position !== 'inline') {
      position = 'fixed';
    }

    var spinner =
    '<div id="'+ id + '" class="spinner-container active ' + position + '">' +
    '<div class="spinner"></div>' +
    '<div class="spinner-content"></div>' +
    '<div class="spinner-backdrop"></div>' +
    '</div>';

    setTimeout(function() {
      if (position === 'fixed') {
        $target.append( spinner );
      } else {
        $target.after( spinner );
      }

      $('#' + id).fadeIn();
    }, 10);

  },

  hideSpinner = function (id) {
    var $spinner = $('#' + id );
    $spinner.stop().fadeOut(function() {
      $spinner.remove();
    });
  },

  toggleSpinner = function (id, $target, position) {
    if ( $('#' + id ).length !== 0 ) {
      this.hideSpinner(id);
    } else {
      this.showSpinner(id, $target, position);
    }
  };

  window.ZK = window.ZK || {};
  window.ZK.showSpinner = showSpinner;
  window.ZK.hideSpinner = hideSpinner;
  window.ZK.toggleSpinner = toggleSpinner;

})(jQuery, window, document);


;(function($, window, document, undefined) {
  'use strict';

  var spinnerObj;

  var spinnerDefault = {
    // lines: 13, // The number of lines to draw
    // length: 28, // The length of each line
    // width: 14, // The line thickness
    // radius: 42, // The radius of the inner circle
    // scale: 1, // Scales overall size of the spinner
    // corners: 1, // Corner roundness (0..1)
    // color: '#000', // #rgb or #rrggbb or array of colors
    // opacity: 0.25, // Opacity of the lines
    // rotate: 0, // The rotation offset
    // direction: 1, // 1: clockwise, -1: counterclockwise
    // speed: 1, // Rounds per second
    // trail: 60, // Afterglow percentage
    // fps: 20, // Frames per second when using setTimeout() as a fallback for CSS
    zIndex: 20, // The z-index (defaults to 2000000000)
    // className: 'spinner', // The CSS class to assign to the spinner
    top: false, // Top position relative to parent
    left: false, // Left position relative to parent
    // shadow: true, // Whether to render a shadow
    // hwaccel: false, // Whether to use hardware acceleration
    position: false, // Element positioning
  };

  var showContentSpinner = function (parentEl) {
    var $parentEl = $(parentEl);

    if ($parentEl.children('.spinner').length >= 0) {
      return;
    }

    var spinnerObj = new Spinner().spin();

    $parentEl.append(spinnerObj.el);
  };

  var hideContentSpinner = function() {
    var spinnerContainer;

    if (this._spinnerObj.el) {
      spinnerContainer = $(this._spinnerObj.el).parent('.spinner-container');

      this._spinnerObj.stop();
      spinnerContainer.remove();
    }
  };

  var toggleContentSpinner = function() {

  };

  window.ZK = window.ZK || {};
  window.ZK.showContentSpinner = showContentSpinner;
  window.ZK.hideContentSpinner = hideContentSpinner;
  window.ZK.toggleContentSpinner = toggleContentSpinner;

})(jQuery, window, document);
