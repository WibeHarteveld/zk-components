(function ($) {
  const $document = $(document);

  const popoverTemplate =
    `<div class="c-popover popover top">
      <div class="c-popover__arrow arrow"></div>
      <div class="c-popover__heading">
        <h3 class="c-popover__title popover-title"></h3>
      </div>
      <div class="c-popover__body popover-content"></div>
    </div>`;

  const popoverTemplateWhite =
    `<div class="c-popover c-popover--white popover top">
      <div class="c-popover__arrow arrow"></div>
      <div class="c-popover__heading">
        <h3 class="c-popover__title popover-title"></h3>
      </div>
      <div class="c-popover__body popover-content"></div>
    </div>`;

  // Initialize popovers
  // 1. Enable dynamic HTML content to have popovers added.
  $document.popover({
    selector: '.js-popover', // [1]
    template: popoverTemplate,
  });

  // White variant
  $('.js-popover-white').popover({
    template: popoverTemplateWhite,
  });
}(jQuery));
