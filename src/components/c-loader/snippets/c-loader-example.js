(function () {
  const pageLoader = new Loader('.js-loader');

  const btnLoaderToggle = document.querySelectorAll('.js-toggle-loader')[0];
  const btnLoaderShow = document.querySelectorAll('.js-show-loader')[0];
  const btnLoaderHide = document.querySelectorAll('.js-hide-loader')[0];

  btnLoaderToggle.addEventListener('click', function () {
    pageLoader.toggle();
  }, false);

  btnLoaderShow.addEventListener('click', function () {
    pageLoader.show();
  }, false);

  btnLoaderHide.addEventListener('click', function () {
    pageLoader.hide();
  }, false);

  console.log(pageLoader);
}());
