// jQuery
//= include jquery/dist/jquery.js

// Bootstrap
//= include bootstrap-sass/assets/javascripts/bootstrap/transition.js
//= include bootstrap-sass/assets/javascripts/bootstrap/alert.js
//= include bootstrap-sass/assets/javascripts/bootstrap/button.js
// include bootstrap-sass/assets/javascripts/bootstrap/carousel.js
//= include bootstrap-sass/assets/javascripts/bootstrap/collapse.js
//= include bootstrap-sass/assets/javascripts/bootstrap/dropdown.js
//= include bootstrap-sass/assets/javascripts/bootstrap/modal.js
//= include bootstrap-sass/assets/javascripts/bootstrap/tab.js
// include bootstrap-sass/assets/javascripts/bootstrap/affix.js
// include bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js
//= include bootstrap-sass/assets/javascripts/bootstrap/tooltip.js
//= include bootstrap-sass/assets/javascripts/bootstrap/popover.js

// Polyfills
//= include svg4everybody/dist/svg4everybody.js

// Vendor
//= include jquery-match-height/dist/jquery.matchHeight.js
// include bootstrap-tour/build/js/bootstrap-tour.js

// Components
//= include spin/dist/spin.js
//= include c-spinner/c-spinner.js
//= include c-loader/c-loader.js
//= include c-tooltip/c-tooltip.js
//= include c-popover/c-popover.js

(function ($) {
  const $document = $(document);

  // SVG Polyfill
  svg4everybody();

  // Initialize tooltips
  // 1. Enable dynamic HTML content to have tooltips added.
  // 2. To avoid unwanted side effects when using tooltips on elements within a
  //    .btn-group, .input-group or on table-related elements.
  $document.tooltip({
    selector: '[data-toggle="tooltip"]', // [1]
    viewport: 'body', // [2]
  });

  // Invoke a responsive equal heights plugin for jQuery
  $('.js-equal-height').matchHeight();

  // If browser lacks flexbox support use CSS float fallback and make equal height
  $('.no-flexbox .o-layout__item--flex, .flexboxtweener .o-layout__item--flex').matchHeight();

  // Accordion fix for this bug:
  // http://stackoverflow.com/questions/19425165/bootstrap-3-accordion-button-toggle-data-parent-not-working
  $document.on('show.bs.collapse', '.js-accordion-independent', function cbCollapse() {
    const $this = $(this);
    const $actives = $this.find('.js-collapse.in, .js-collapse.collapsing');

    $actives.each((index, element) => {
      $(element).collapse('hide');
    });
  });

}(jQuery));
