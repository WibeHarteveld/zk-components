(function ($) {
  const template =
    `<div class="c-popover c-popover--tour popover top">
      <div class="c-popover__arrow arrow"></div>
      <div class="c-popover__heading c-popover__heading--with-icon">
        <div class="o-layout--table o-layout--va-middle">
          <div class="o-layout__item--cell o-layout__item--icon u-padding-right-tiny">
            <img class="c-popover__icon" src="/assets/img/lightbulb.svg" alt="">
          </div>
          <div class="o-layout__item--cell">
            <h3 class="c-popover__title popover-title"></h3>
          </div>
        </div>
      </div>
      <div class="c-popover__body popover-content"></div>
      <div class="c-popover__footer">
        <div class="clearfix u-margin-bottom-small">
          <a href="" class="c-btn c-btn--outline c-btn--outline-white btn pull-left" data-role="prev">
            <i class="u-icon u-icon-arrow-left" aria-hidden="true"></i>
            Vorige
          </a>
          <a href="" class="c-btn c-btn--outline c-btn--outline-white btn pull-right" data-role="next">
            Volgende
            <i class="u-icon u-icon-arrow-right" aria-hidden="true"></i>
          </a>
        </div>
        <a href="" class="c-popover__link" data-role="end">Bedankt voor de tip</a>
      </div>
    </div>`;

  const tours = {
    tour1: new Tour({
      name: 'kdTour1',
      storage: false,
      template,
      steps: [
        {
          element: '.js-tour-step-behandeljaar',
          placement: 'bottom',
          title: 'Nieuw! Declaraties op behandeljaar filteren',
          content: 'Filter hier uw declaraties op behandeljaar. Een behandeljaar is een jaar waarin u een behandeling heeft gehad.',
        }, {
          element: '.js-tour-step-indiener',
          placement: 'bottom',
          title: 'Nieuw! Filteren op de indiener van de declaraties',
          content: 'Voortaan kunt u hier filteren op declaraties die uzelf of uw zorgverlener heeft ingediend. Of u bekijkt beide.',
        }, {
          element: '.js-tour-step-eigen-risico',
          placement: 'bottom',
          title: 'Nieuw! Declaraties met eigen risico bekijken',
          content: 'Wilt u alleen uw declaraties waarover u eigen risico betaalt? Zet dan hier een vinkje.',
        },
      ],
    }),
  };

  const startTour = (tour) => {
    tour.init();
    tour.start();
  };

  $('.js-tour-start').click(() => {
    startTour(tours.tour1);
  });
}(jQuery));
