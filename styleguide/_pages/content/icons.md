---
template: docs.pug
title: Icons
---
# Icons
Use them in buttons, button groups for a toolbar, navigation, or prepended form inputs.
The utility classes are based on the [fontawesome.io](http://fontawesome.io/examples/) font toolkit.

## Available ZK font icons

<div class="sg-zkicons__list">
  <div class="row">
    <div class="col-sm-8">
      <ul class="c-list c-list--unstyled">
        <li>
          <i class="u-icon u-icon-calendar" aria-hidden="true"></i>
          <span class="glyphicon-class">calendar</span>
        </li>
        <li>
          <i class="u-icon u-icon-eye" aria-hidden="true"></i>
          <span class="glyphicon-class">eye</span>
        </li>
        <li>
          <i class="u-icon u-icon-wallet" aria-hidden="true"></i>
          <span class="glyphicon-class">wallet</span>
        </li>
        <li>
          <i class="u-icon u-icon-file-stack" aria-hidden="true"></i>
          <span class="glyphicon-class">file-stack</span>
        </li>
        <li>
          <i class="u-icon u-icon-pie-euro" aria-hidden="true"></i>
          <span class="glyphicon-class">pie-euro</span>
        </li>
        <li>
          <i class="u-icon u-icon-question" aria-hidden="true"></i>
          <span class="glyphicon-class">question</span>
        </li>
        <li>
          <i class="u-icon u-icon-option-horizontal" aria-hidden="true"></i>
          <span class="glyphicon-class">option-horizontal</span>
        </li>
        <li>
          <i class="u-icon u-icon-mobile-arrow" aria-hidden="true"></i>
          <span class="glyphicon-class">mobile-arrow</span>
        </li>
        <li>
          <i class="u-icon u-icon-plus-sign" aria-hidden="true"></i>
          <span class="glyphicon-class">plus-sign</span>
        </li>
        <li>
          <i class="u-icon u-icon-facebook" aria-hidden="true"></i>
          <span class="glyphicon-class">facebook</span>
        </li>
        <li>
          <i class="u-icon u-icon-marker-zk" aria-hidden="true"></i>
          <span class="glyphicon-class">marker-zk</span>
        </li>
        <li>
          <i class="u-icon u-icon-twitter" aria-hidden="true"></i>
          <span class="glyphicon-class">twitter</span>
        </li>
        <li>
          <i class="u-icon u-icon-mobile" aria-hidden="true"></i>
          <span class="glyphicon-class">mobile</span>
        </li>
        <li>
          <i class="u-icon u-icon-envelope" aria-hidden="true"></i>
          <span class="glyphicon-class">envelope</span>
        </li>
      </ul>
    </div>
    <div class="col-sm-8">
      <ul class="c-list c-list--unstyled">
        <li>
          <i class="u-icon u-icon-conversation" aria-hidden="true"></i>
          <span class="glyphicon-class">conversation</span>
        </li>
        <li>
          <i class="u-icon u-icon-arrow-right" aria-hidden="true"></i>
          <span class="glyphicon-class">arrow-right</span>
        </li>
        <li>
          <i class="u-icon u-icon-arrow-left" aria-hidden="true"></i>
          <span class="glyphicon-class">arrow-left</span>
        </li>
        <li>
          <i class="u-icon u-icon-arrow-down" aria-hidden="true"></i>
          <span class="glyphicon-class">arrow-down</span>
        </li>
        <li>
          <i class="u-icon u-icon-arrow-up" aria-hidden="true"></i>
          <span class="glyphicon-class">arrow-up</span>
        </li>
        <li>
          <i class="u-icon u-icon-search" aria-hidden="true"></i>
          <span class="glyphicon-class">search</span>
        </li>
        <li>
          <i class="u-icon u-icon-user" aria-hidden="true"></i>
          <span class="glyphicon-class">user</span>
        </li>
        <li>
          <i class="u-icon u-icon-cross-sign" aria-hidden="true"></i>
          <span class="glyphicon-class">cross-sign</span>
        </li>
        <li>
          <i class="u-icon u-icon-ok" aria-hidden="true"></i>
          <span class="glyphicon-class">ok</span>
        </li>
        <li>
          <i class="u-icon u-icon-info-sign" aria-hidden="true"></i>
          <span class="glyphicon-class">info-sign</span>
        </li>
        <li>
          <i class="u-icon u-icon-lock-open" aria-hidden="true"></i>
          <span class="glyphicon-class">lock-open</span>
        </li>
        <li>
          <i class="u-icon u-icon-pencil" aria-hidden="true"></i>
          <span class="glyphicon-class">pencil</span>
        </li>
        <li>
          <i class="u-icon u-icon-comment" aria-hidden="true"></i>
          <span class="glyphicon-class">comment</span>
        </li>
        <li>
          <i class="u-icon u-icon-clock" aria-hidden="true"></i>
          <span class="glyphicon-class">clock</span>
        </li>
      </ul>
    </div>
    <div class="col-sm-8">
      <ul class="c-list c-list--unstyled">
        <li>
          <i class="u-icon u-icon-cross" aria-hidden="true"></i>
          <span class="glyphicon-class">cross</span>
        </li>
        <li>
          <i class="u-icon u-icon-marker" aria-hidden="true"></i>
          <span class="glyphicon-class">marker</span>
        </li>
        <li>
          <i class="u-icon u-icon-comment3" aria-hidden="true"></i>
          <span class="glyphicon-class">comment3</span>
        </li>
        <li>
          <i class="u-icon u-icon-star" aria-hidden="true"></i>
          <span class="glyphicon-class">star</span>
        </li>
        <li>
          <i class="u-icon u-icon-ok-trans-marker" aria-hidden="true"></i>
          <span class="glyphicon-class">ok-trans-marker</span>
        </li>
        <li>
          <i class="u-icon u-icon-question-sign" aria-hidden="true"></i>
          <span class="glyphicon-class">question-sign</span>
        </li>
        <li>
          <i class="u-icon u-icon-plus-marker" aria-hidden="true"></i>
          <span class="glyphicon-class">plus-marker</span>
        </li>
        <li>
          <i class="u-icon u-icon-ok-sign" aria-hidden="true"></i>
          <span class="glyphicon-class">ok-sign</span>
        </li>
        <li>
          <i class="u-icon u-icon-cross-trans-marker" aria-hidden="true"></i>
          <span class="glyphicon-class">cross-trans-marker</span>
        </li>
        <li>
          <i class="u-icon u-icon-file" aria-hidden="true"></i>
          <span class="glyphicon-class">file</span>
        </li>
        <li>
          <i class="u-icon u-icon-external-link" aria-hidden="true"></i>
          <span class="glyphicon-class">external-link</span>
        </li>
        <li>
          <i class="u-icon u-icon-linkedin" aria-hidden="true"></i>
          <span class="glyphicon-class">linkedin</span>
        </li>
        <li>
          <i class="u-icon u-icon-print" aria-hidden="true"></i>
          <span class="glyphicon-class">print</span>
        </li>
        <li>
          <i class="u-icon u-icon-whatsapp" aria-hidden="true"></i>
          <span class="glyphicon-class">whatsapp</span>
        </li>
      </ul>
    </div>
  </div>
</div>

## Add/remove icons in font
The icon font is generated with [icomoon.io](https://icomoon.io/) and is called zk-icons-font.

## Basic icons

## Sizing
Change the icon size with the class `.u-icon-*x`

<ul class="c-list c-list--unstyled">
  <li>
    <i class="u-icon u-icon-pie-euro u-color-blue-8" aria-hidden="true"></i>
    Normal
  </li>
  <li>
    <i class="u-icon u-icon-pie-euro u-icon-large u-color-blue-8" aria-hidden="true"></i>
    Makes the font 33% larger relative to the icon container
  </li>
  <li>
    <i class="u-icon u-icon-pie-euro u-icon-2x u-color-blue-8" aria-hidden="true"></i>
    u-icon-2x
  </li>
  <li>
    <i class="u-icon u-icon-pie-euro u-icon-3x u-color-blue-8" aria-hidden="true"></i>
    u-icon-3x
  </li>
  <li>
    <i class="u-icon u-icon-pie-euro u-icon-4x u-color-blue-8" aria-hidden="true"></i>
    u-icon-4x
  </li>
  <li>
    <i class="u-icon u-icon-pie-euro u-icon-5x u-color-blue-8" aria-hidden="true"></i>
    u-icon-5x
  </li>
</ul>

## Fixed width icons
Use `.u-icon-fw` to set icons at a fixed width. Great to use when different icon widths throw off alignment. Especially useful in things like nav lists & list groups.

<div>
  <i class="u-icon u-icon-pie-euro u-icon-2x u-icon-fw u-color-blue-8" aria-hidden="true"></i>
  Eigen risico: nog € 139,46 over
</div>

## Vertical aligned icon
Vertical align (absolute positioning) an icon with the class `.u-icon-va`.
The container needs to be `position: relative`.

## List icons

## Bordered & pulled icons

## Animated icons
Use the `.u-icon-spin` class to get any icon to rotate, and use `.u-icon-pulse` to have it rotate with 8 steps. Works well with fa-spinner, fa-refresh, and fa-cog.

## Rotated & flipped
To arbitrarily rotate and flip icons, use the `.u-icon-rotate-*` and `.u-icon-flip-*` classes.

<ul class="c-list c-list--unstyled">
  <li>
    <i class="u-icon u-icon-mobile" aria-hidden="true"></i>
    normal
  </li>
  <li>
    <i class="u-icon u-icon-mobile u-icon-rotate-90" aria-hidden="true"></i>
    u-icon-rotate-90
  </li>
  <li>
    <i class="u-icon u-icon-mobile u-icon-rotate-180" aria-hidden="true"></i>
    u-icon-rotate-180
  </li>
  <li>
    <i class="u-icon u-icon-mobile u-icon-rotate-270" aria-hidden="true"></i>
    u-icon-rotate-270
  </li>
  <li>
    <i class="u-icon u-icon-mobile u-icon-flip-horizontal" aria-hidden="true"></i>
    u-icon-flip-horizontal
  </li>
  <li>
    <i class="u-icon u-icon-mobile u-icon-flip-vertical" aria-hidden="true"></i>
    u-icon-flip-vertical
  </li>
</ul>

## Accessibility-minded
If an icon only adds some extra decoration or branding, it does not need to be announced to users as they are navigating your site or app aurally. Alternatively, if an icon conveys meaning in your content or interface, ensure that this meaning is also conveyed to assistive technologies through alternative displays or text.
