---
template: docs.pug
title: Typography
---
# Typography

## Headings

All HTML headings, `<h1>` through `<h6>`, are available.

|                                |                       |
| -------------------------------|:---------------------:|
| <h1>h1. Bootstrap heading</h1> | semi-bold (600), 42px |
| <h2>h2. Bootstrap heading</h2> | normal (400), 28px |
| <h3>h3. Bootstrap heading</h3> | normal (400), 26px |
| <h4>h4. Bootstrap heading</h4> | semi-bold (600), 22px |
| <h5>h5. Bootstrap heading</h5> | semi-bold (600), 18px |
| <h6>h6. Bootstrap heading</h6> | semi-bold (600), 18px |

<div class="sg-example sg-example--type">

  <table class="table">
    <tbody>
      <tr>
        <td><h1></h1></td>
        <td class="type-info">Semibold 36px</td>
      </tr>
      <tr>
        <td><h2>h2. Bootstrap heading</h2></td>
        <td class="type-info">Semibold 30px</td>
      </tr>
      <tr>
        <td><h3>h3. Bootstrap heading</h3></td>
        <td class="type-info">Semibold 24px</td>
      </tr>
      <tr>
        <td><h4>h4. Bootstrap heading</h4></td>
        <td class="type-info">Semibold 18px</td>
      </tr>
      <tr>
        <td><h5>h5. Bootstrap heading</h5></td>
        <td class="type-info">Semibold 14px</td>
      </tr>
      <tr>
        <td><h6>h6. Bootstrap heading</h6></td>
        <td class="type-info">Semibold 12px</td>
      </tr>
    </tbody>
  </table>

</div>

## Lists

### Unstyled
<div class="sg-example sg-example--type">

  <ul class="list-unstyled">
    <li>Lorem ipsum dolor sit amet</li>
    <li>Consectetur adipiscing elit</li>
    <li>Integer molestie lorem at massa</li>
    <li>Facilisis in pretium nisl aliquet</li>
    <li>Nulla volutpat aliquam velit
      <ul>
        <li>Phasellus iaculis neque</li>
        <li>Purus sodales ultricies</li>
        <li>Vestibulum laoreet porttitor sem</li>
        <li>Ac tristique libero volutpat at</li>
      </ul>
    </li>
    <li>Faucibus porta lacus fringilla vel</li>
    <li>Aenean sit amet erat nunc</li>
    <li>Eget porttitor lorem</li>
  </ul>

</div>

### Inline
<div class="sg-example sg-example--type">

  <ul class="list-inline">
    <li class="list-inline-item">Lorem ipsum</li>
    <li class="list-inline-item">Phasellus iaculis</li>
    <li class="list-inline-item">Nulla volutpat</li>
  </ul>

</div>

### Description list
<div class="sg-example sg-example--type">

  <dl>
    <dt>Description lists</dt>
    <dd>A description list is perfect for defining terms.</dd>
    <dt>Euismod</dt>
    <dd>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
    <dd>Donec id elit non mi porta gravida at eget metus.</dd>
    <dt>Malesuada porta</dt>
    <dd>Etiam porta sem malesuada magna mollis euismod.</dd>
  </dl>

</div>

#### With bullets
<div class="sg-example sg-example--type">

  <dl class="c-list c-list--icon c-list--icon-bullet">
    <div class="c-list__box">
      <dt class="c-list__item">Description lists</dt>
      <dd>A description list is perfect for defining terms.</dd>
    </div>
    <div class="c-list__box">
      <dt class="c-list__item">Euismod</dt>
      <dd>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
      <dd>Donec id elit non mi porta gravida at eget metus.</dd>
    </div>
    <div class="c-list__box">
      <dt class="c-list__item">Malesuada porta</dt>
      <dd>Etiam porta sem malesuada magna mollis euismod.</dd>
    </div>
  </dl>

</div>

#### Horizontally aligned (fixed `<dt>` width)
<div class="sg-example sg-example--type">

  <dl class="c-list c-list--description dl-horizontal">
    <dt class="c-list__item">Description lists</dt>
    <dd>A description list is perfect for defining terms.</dd>
    <dt class="c-list__item">Euismod</dt>
    <dd>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
    <dd>Donec id elit non mi porta gravida at eget metus.</dd>
    <dt class="c-list__item">Malesuada porta</dt>
    <dd>Etiam porta sem malesuada magna mollis euismod.</dd>
    <dt class="c-list__item">Malesuada porta lorem eget metus</dt>
    <dd>Etiam porta sem malesuada magna mollis euismod.</dd>
  </dl>

</div>

#### Horizontally aligned (with cols)
<div class="sg-example sg-example--type">

  <dl class="c-list c-list--description">
    <div class="c-list__box row">
      <dt class="c-list__item c-list__item--term col-sm-8 col-lg-6">Rekeningnummer</dt>
      <dd class="c-list__item c-list__item--description col-sm-16 col-md-14">NL 00 ABCD 0123 4567 89</dd>
    </div>
    <div class="c-list__box row">
      <dt class="c-list__item c-list__item--term col-sm-8 col-lg-6">Betaalwijze</dt>
      <dd class="c-list__item c-list__item--description col-sm-16 col-md-14">Automatische incasso</dd>
    </div>
    <div class="c-list__box row">
      <dt class="c-list__item c-list__item--term col-sm-8 col-lg-6">Voorkeur incassodag</dt>
      <dd class="c-list__item c-list__item--description col-sm-16 col-md-14">24e van de maand</dd>
    </div>
    <div class="c-list__box row">
      <dt class="c-list__item c-list__item--term col-sm-8 col-lg-6">Betaaltermijn</dt>
      <dd class="c-list__item c-list__item--description col-sm-16 col-md-14">Per maand <strong class="u-color-orange-1">(vanaf 01-04-2017 betaalt u per kwartaal)</strong></dd>
    </div>
    <div class="c-list__box row">
      <dt class="c-list__item c-list__item--term col-sm-8 col-lg-6">E-mailadres</dt>
      <dd class="c-list__item c-list__item--description col-sm-16 col-md-14">naam@mail.nl</dd>
    </div>
  </dl>

</div>
