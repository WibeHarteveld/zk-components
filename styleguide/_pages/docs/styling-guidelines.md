---
title: Styling guidelines
template: docs.pug
---

# Styling guidelines

## Structure with ITCSS
- [ITCSS: scalable and maintainable css architecture](https://www.xfive.co/blog/itcss-scalable-maintainable-css-architecture/)
- [Manage large CSS projects with ITCSS](http://www.creativebloq.com/web-design/manage-large-css-projects-itcss-101517528)
- [Harry Roberts managing CSS Projects with ITCSS](https://www.youtube.com/watch?v=1OKZOV-iLj4&t=3093s)
- [inuitcss](https://github.com/inuitcss/inuitcss)
- [The Specificity Graph](http://csswizardry.com/2014/10/the-specificity-graph/)

ITCSS stands for Inverted Triangle Cascading Style Sheet. This methodology allocates and groups your styles based on their purpose, and arranges them in order of specificity and range of influence (as per the inverted triangle metaphor).

![ITCSS key metrics](/assets/img/styleguide/itcss-key-metrics.svg)

![ITCSS layers](/assets/img/styleguide/itcss-layers.svg)

![ITCSS layers](/assets/img/styleguide/specificity-graph-02.png)

- Write CSS in **specificity order**.
- Maintain the **Specificity Graph**.
- All rulesets should only ever **add to and inherit from previous ones**.
- Order stylesheets **from far reaching to very localised**.

## Components
> Components can be anything, as long as they do one thing and one thing only, re-usable, re-used across the project and most importantly independent;

## BEM
- [BEM - MindBemding - getting your head round BEM syntax](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/)
- [BEM - Battling BEM: 10 common problems and how to avoid them](https://www.smashingmagazine.com/2016/06/battling-bem-extended-edition-common-problems-and-how-to-avoid-them/)
- [Better CSS: Putting it together with Atomic, ITCSS & BEM](https://www.silverstripe.org/blog/better-css-putting-it-together-with-atomic-itcss-and-bem/)
- [BEM & Atomic Design: A CSS Architecture Worth Loving](https://www.lullabot.com/articles/bem-atomic-design-a-css-architecture-worth-loving)

## Style conventions
[CSS guidelines reference 1](https://github.com/chris-pearce/css-guidelines)
[CSS guidelines reference 2](http://cssguidelin.es/)

- Variables should follow the `$component-state-property-size` formula for consistent naming. Ex: $c-nav-link-is-disabled-color and $c-modal-content-box-shadow-xs.
- Don't use camelCase;
- Don't use underscores with the exception of being used in BEM element selectors;
- Avoid IDs as styling hooks
- Avoid the descendent selector (i.e. don’t use `.sidebar h3`)
- Avoid attaching classes to elements in your stylesheet (i.e. don’t do `div.header` or `h1.title`)
- Avoid using `!important` except in some rare cases (utility classes)

## Using Bootstrap
We make use of the Bootstrap 3 framework. This framework can't be touched except by altering the variable values in `src/settings/_settings.bs.scss`. This enables easy bootstrap upgrade transitions without the need to diff code all over the place.

- Structure wise, `_settings.bs.scss` needs to be an exact copy of the standard bootstrap variables file (keep this in mind when upgrading bootstrap).
- When changing variables within `_settings.bs.scss`, remove `!default` so we can easily identify what we have changed.
- Always use BEM classes to overrule bootstrap styling, **do not** use bootstrap's OOCSS naming convention.
- Prepend BEM classes before standard bootstrap classes in the html class attributes .e.g. `class="c-panel panel"`.

## Namespaces
[Source 1: Namespaces](http://csswizardry.com/2015/03/more-transparent-ui-code-with-namespaces/#the-namespaces)
Namespaces make the code more readable:

| Type     | Prefix | examples     |
| ---------|:------:| ------------:|
| Object   | `o-`   | `o-box`                |
| Component| `c-`   | `c-btn c-btn--primary` |
| Utility  | `u-`   | `u-clearfix`           |
| Template | `tp-`   | `tp-subpage`            |
| Theme    | `th-`   | `th-interpolis`       |
| State    | `is-, has-` | `is-active`                 |
| JS hook  | `js-`  | `js-drop-down-menu-close-button` |
| Hack     | `_`    | `_c-footer-mobile`               |

### Object namespace: `o-`
Signify that something is an Object, and that it may be used in any number of unrelated contexts to the one you can currently see it in. Making modifications to these types of class could potentially have knock-on effects in a lot of other unrelated places. Tread carefully.

### Component namespace: `c-`
Signify that something is a Component. This is a concrete, implementation-specific piece of UI. All of the changes you make to its styles should be detectable in the context you’re currently looking at. Modifying these styles should be safe and have no side effects.

### Utility namespace `u-`
It has a very specific role (often providing only one declaration) and should not be bound onto or changed. It can be reused and is not tied to any specific piece of UI.

### Template namespace `tp-`
Signify that a class is responsible for adding a template to a view.
It lets us know that UI Components' current positional appearance may be due to the presence of a template. E.g. different header positioning or full-width background pictures.

### Theme namespace `th-`
Signify that a class is responsible for adding a theme to a view. It lets us know that UI Components' current cosmetic appearance may be due to the presence of a theme. E.g. different label colors for a particular label (i.e. Interpolis, CB, ZK).

### State namespace `is-, has-`
Signify that the piece of UI in question is currently styled a certain way because of a state or condition. It tells us that the DOM currently has a temporary, optional, or short-lived style applied to it due to a certain state being invoked. State styles indicate a JavaScript dependency

The convention we use for [state hooks](https://github.com/chris-pearce/css-guidelines#state-hooks).

### JS hook namespace `js-`
Signify that this piece of the DOM has some behavior acting upon it, and that JavaScript binds onto it to provide that behavior.

As a rule, it is unwise to bind your CSS and your JS onto the same class in your HTML. This is because doing so means you can’t have (or remove) one without (removing) the other.

- These classes must be explicitly used for JS binding purposes, **don't** style on these.
- Use the naming convention [JS hooks](https://github.com/chris-pearce/css-guidelines#js-hooks)
- Only use `data-*` attributes to store custom data private to the page (as per the spec). **Don't** use this attribute for JS binding purposes.

### Hack namespace `_`
Signify that this class is the worst of the worst—a hack! Sometimes, although incredibly rarely, we need to add a class in our markup in order to force something to work. If we do this, we need to let others know that this class is less than ideal, and hopefully temporary (i.e. do not css bind onto this).

## SASS nesting
CSS nesting must be avoided most of the time, for some edge cases we nest rules for easier maintenance. E.g. for component color variants we use SASS' parent selector to prepend `.c-panel--bare` onto the existing `.c-panel__heading {}` selector. This means that all of our `.c-panel__heading {}`-related rules exist in one place, and aren’t spread throughout the file. This is general good practice when dealing with nested code: keep all of your context (e.g. all `.c-panel__heading {}` code) encapsulated in one location.

```SCSS
.c-panel__heading {
  position: relative;
  background-color: $color-blue-1;
  border-color: $color-blue-1;
  color: $color-blue-5;

  .c-panel--bare & {
    background-color: $color-white;
    border-color: $color-white;
  }
}
```
Output CSS:
```CSS
.c-panel__heading {
  position: relative;
  background-color: #d6eff0;
  border-color: #d6eff0;
  color: #0073cf;
}
.c-panel--bare .c-panel__heading {
  background-color: #fff;
  border-color: #fff;
}
```

## Intersection of components
The cosmetics of a truly modular UI element should be totally agnostic of the element’s parent container — it should look the same regardless of where you drop it. Adding a class from another component for bespoke styling, as the ["mix"](https://en.bem.info/forum/4/) approach does, violates the [open/closed](https://en.wikipedia.org/wiki/Open/closed_principle) principle of component-driven design — i.e there should be no dependency on another module for aesthetics.

E.g. don't do this
```html
<button class="c-btn o-box__c-btn">Click me!</button>
```
Use a modifier for these small cosmetic differences, because you may well find that you wish to reuse them elsewhere as the project grows.
```html
<button class="c-btn c-btn--rounded c-btn--small">Click me!</button>
```

## Theming
Theming can be used to style components which are more inline with styling of other labels like FBTO, Interpolis etc. Theming abilities are scoped on color variations only!

### Theme color pallet
All theming color variables are declared within a color palette settings file `src/settings/_settings.colors.[theme].scss`. This file includes all colors for a specific theme style (e.g. a color palette for the FBTO label).

**Rules**
- Use sass maps for color palette management derived from `_settings.colors.scss`

### Component settings file
Each component has a settings file `src/components/[component]/[component].settings.scss`. This file corresponds to all variables which are included within the component style file `src/components/[component]/[component].scss` The variables can be overruled with one (or more) component theme settings files (see: Component theme settings files).

**Rules**
- Always add `!default` in the variable declaration when adding variables to this file, so that theme specific stylesheets can overrule these settings.
- Variables should follow the `$component-state-property-size` formula for consistent naming. Ex: `$c-nav-link-is-disabled-color` and `$c-modal-content-box-shadow-xs`.

### Component theme settings files
This file is located at: `src/components/[component]/[component].settings.[theme].scss`.
This file contains variables which overrule the component settings file variables.

**Rules**
- This file only contains variables copied from the component settings file.

### Theme component**s** file
The `src/components/_components.settings.[theme].scss` file imports all individual theme component settings.
This is a convenient way for adding all component theme settings with one import in a bootstrap style file e.g. `[theme]-klantdomein.scss`.

## Linting
This project has two linters integrated:
- stylelint: for SASS & CSS linting
- eslint: for javascript linting ([see javascript guidelines](./javascript-guidelines.html))

The [stylelint configuration](https://github.com/stylelint/stylelint-config-standard) is derived from the common rules found within a handful of CSS styleguides, including: [The Idiomatic CSS Principles](https://github.com/necolas/idiomatic-css),
[Github's PrimerCSS Guidelines](http://primercss.io/guidelines/#scss),
[Google's CSS Style Guide](https://google.github.io/styleguide/htmlcssguide.xml#CSS_Formatting_Rules), [Airbnb's Styleguide](https://github.com/airbnb/css#css), and [@mdo's Code Guide](http://codeguide.co/#css).

It favours flexibility over strictness for things like multi-line lists and single-line rulesets, and tries to avoid potentially divisive rules.

## Breakpoints
When using breakpoints, try to use the mobile-first mixin variant (media-breakpoint-up).

Use mixins to add breakpoints e.g.
```SCSS
@include media-breakpoint-up(sm) {
  .c-panel {
    font-size: $global-font-size-base;
  }
}

@include media-breakpoint-down(sm) {
  .c-panel {
    font-size: $global-font-size-base;
  }
}
```

Compiled CSS:

```CSS
@media (min-width: 768px) {
  .c-panel {
    font-size: 16px;
  }
}

@media (max-width: 767px) {
  .c-panel {
    font-size: 16px;
  }
}
```

## Transition effect
TODO: add docs
