---
title: Getting started
template: docs.pug
---

# Getting started

## Principle and rules
Before you begin developing, you need to know about the rules and principles we follow. Because each developer brings there own code style, we have rules to preserve an uniform/easy maintainable codebase. You need to comply with these rules when contributing to the source code.
- [Architectural principles](./architectural-principles.html)
- [Styling guidelines](./styling-guidelines.html)
- [javascript guidelines](./javascript-guidelines.html)
- [Versioning guidelines](./versioning-guidelines.html)

## Installing dependencies
The following need to be installed on your system:
1. Install node v6 or higher
1. Clone this repo using `git clone https://...`
1. Install gulp-cli globally using `npm install -g gulp-cli`
1. Install project dependencies with `npm install`

## Project CLI commands
### Development
Initialize project and start server (with browserSync):
```shell
gulp start
```
Start server (with browserSync):
```shell
gulp serve
```
Build dist folder:
```shell
gulp build
```
### Bundle
TODO: write docs about sharepoint integration

## Linters
This project has two linters integrated:
- stylelint: for SASS & CSS linting
- eslint: for javascript linting

You can lint your code by running:
```shell
gulp lint
```

It is also a good idea to install linting plugins based on stylelint and eslint in your IDE, so you can instantly identify
linting problems.
