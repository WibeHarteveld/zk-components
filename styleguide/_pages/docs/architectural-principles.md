---
title: Architectural Principles
template: docs.pug
---

# Architectural Principles

## OOCSS
- [An introduction to Object-oriented CSS](https://www.smashingmagazine.com/2011/12/an-introduction-to-object-oriented-css-oocss/)
- [The single responsibility principle applied to css](http://csswizardry.com/2012/04/the-single-responsibility-principle-applied-to-css/)

> Basically, a CSS “object” is a repeating visual pattern, that can be abstracted into an independent snippet of HTML, CSS, and possibly javascript. That object can then be reused throughout a site.

The two principles of OOCSS:
- Separation of structure from skin
- Separation of containers and content

With OOCSS, you're encouraged to give more forethought to what is common among different elements, then separate those common features into modules, or objects, that can be reused anywhere.

## Single Responsibility Principle
The [Single Responsibility Principle](http://cssguidelin.es/#object-orientation) states that every module or chunk of code (a function etc) should do one job well and one job only.

The benefits of this are mainly in the way of maintainability and extensibility.

## Open/Closed Principle
The [Open/Closed Principle](http://cssguidelin.es/#the-openclosed-principle) states that any additions, new functionality, or features we add to our classes should be added via *extension*—we should not modify these classes directly.

Remember: **open for extension; closed for modification**.

## DRY
The [DRY principle](http://cssguidelin.es/#dry) which stands for Don’t Repeat Repeat Yourself, is a micro-principle which aims to keep the repetition of key information to a minimum.

## Composition over Inheritance
The [Composition over Inheritance](http://cssguidelin.es/#composition-over-inheritance) principle suggests that larger systems should be composed from much smaller, individual parts, rather than inheriting behavior from a much larger, monolithic object. This should keep your code decoupled—nothing inherently relies on anything else..

## The Separation of Concerns
The [Separation of Concerns](http://cssguidelin.es/#the-separation-of-concerns) principle stands for building individual components, and writing code which only focusses itself on one task at a time.
