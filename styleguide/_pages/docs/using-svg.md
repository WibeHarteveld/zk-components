---
title: Using SVG
template: docs.pug
---
# Using SVG
[Using SVG](https://css-tricks.com/using-svg/)
[Icon System with SVG Sprites](https://css-tricks.com/svg-sprites-use-better-icon-fonts/)

The SVG system we use is based on the `<symbol>` tag.

Why we use `<symbol>` for SVG sprites instead of `<g>` or `<view>`:
- The viewBox can be defined on the `<symbol>`, so you don't need to use it in the markup (easier and less error prone).
- Title and desc tags can be added within the `<symbol>` and they kinda "come along for the ride" when the symbol gets used, making accessibility easier to do right.
- Symbols don't display as you define them, so no need for a `<defs>` block.
- This is probably what <symbol> was invented for anyway.

## SVG sprite with `<svg>`
[SVG 'symbol' a Good Choice for Icons](https://css-tricks.com/svg-symbol-good-choice-icons/)

Example of how to use the SVG sprite within HTML:

```HTML
<svg>
  <use xlink:href="/assets/img/sprite.main.svg#envelope-open"></use>
</svg>
```

### Manipulate SVG with CSS:

HTML:
```HTML
<svg class="o-box__curved-divider u-scale-svg__img" preserveAspectRatio="xMinYMid">
  <use xlink:href="/assets/img/sprite.main.svg#curved-divider-left"></use>
</svg>
```
SASS:
```SCSS
.o-box__curved-divider {
  fill: $color-white;
}
```

## SVG with `<img>`
<div class="c-alert c-alert--warning alert">**Heads up!** With this approach you can't manipulate SVG with CSS </div>

When using an `<img>` tag, use a separate SVG image e.g. `<img src="/assets/img/envelope-open.svg" alt="">`.
This is needed because using a sprite **isn't** supported in browsers e.g. (**doesn't work**) `<img src="/assets/img/sprite.main.svg#envelope-open" alt="">`.

## SVG in CSS
<div class="c-alert c-alert--warning alert">**Heads up!** Without inline embedding you won’t be able to style SVG from CSS</div>

When using an SVG in CSS, target a separate SVG image e.g.
```CSS
.some-class {
  background: url(/assets/img/curved-divider-left.svg) no-repeat;
}
```

This is needed because using a sprite **isn't** supported in browsers when using a sprite based on `<symbol>`

## Responsive SVG
[How to scale SVG](https://css-tricks.com/scale-svg)

### Auto-scale `<img>`
- Set a viewBox attribute.
- Set at least one of height or width.
- Don't put it inside a table layout (has issues with IE).

### Auto-scale `<svg>`
Use the padding-bottom hack (see utilities.scaling.scss)

### SVG polyfill
[SVG for Everybody](https://github.com/jonathantneal/svg4everybody)

External SVG resources doesn't work in any version (up to 11 tested) of Internet Explorer. Even the ones that do support inline SVG: 9, 10, 11. Therefore we use a polyfill named "SVG for Everybody".

## Accessible SVG's
[Accessible SVG's](https://css-tricks.com/accessible-svgs/)
