import path from 'path';
import del from 'del';
import glob from 'glob';
import mergeStream from 'merge-stream';
import runSequence from 'run-sequence';
import browserSync from 'browser-sync';
import through from 'through2';
import pug from 'pug';
import markdown from 'marked';
import gulp from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';
import autoprefixer from 'autoprefixer';
import cssnano from 'cssnano';
import stylelint from 'stylelint';
import reporter from 'postcss-reporter';
import scss from 'postcss-scss';
import modernizr from 'modernizr';
import pkg from './package.json';

const $ = gulpLoadPlugins();
const { reload } = browserSync;
const banner = ['/**',
  ' * <%= pkg.name %> - <%= pkg.description %>',
  ' * @version v<%= pkg.version %>',
  ' */',
  ''].join('\n');

const svgSpriteConfig = {
  mode: {
    symbol: {
      dest: './',
      sprite: 'sprite.main.svg',
    },
  },
};

const browsersList = [
  'Android 2.3',
  'Android >= 4',
  'Chrome >= 20',
  'Firefox >= 24',
  'Explorer >= 9',
  'iOS >= 6',
  'Opera >= 12',
  'Safari >= 6',
];

const babelConfig = {
  babelrc: false,
  presets: [
    ['env', {
      modules: false,
      targets: {
        browsers: browsersList,
      },
    }],
  ],
};

const modernizrConfig = {
  minify: false,
  options: [
    'setClasses',
    'mq',
  ],
  'feature-detects': [
    'css/flexbox',
    'css/flexboxlegacy',
    'css/flexboxtweener',
    'css/flexwrap',
    'history',
  ],
};

const uglifiyConfig = {};

// Plumber error handling
function errorHandler(error) {
  $.util.log(
    $.util.colors.cyan('Plumber') + $.util.colors.red(' found unhandled error:\n'),
    error.toString(),
  );
  this.emit('end');
}

// Clean Output Directory and gulp cache
gulp.task('clean:dist', () => del(['dist']));
gulp.task('clean:cache', () => $.cache.clearAll());

// Optimize Images
gulp.task('images:components', () =>
  gulp.src(['src/images/**/*.{svg,png,jpg,gif}', 'styleguide/_pages/**/*.{svg,png,jpg,gif}'])
    .pipe($.imagemin({
      progressive: true,
      interlaced: true,
    }))
    .pipe(gulp.dest('dist/assets/img'))
    .pipe($.size({ title: 'images' })),
);

gulp.task('images:svgsprite', () =>
  gulp.src(['src/images/**/*.svg'])
    .pipe($.svgSprite(svgSpriteConfig))
    .pipe(gulp.dest('dist/assets/img')),
);

gulp.task('styles', () =>
  gulp.src('src/**/*.scss')
    .pipe($.plumber({ errorHandler }))
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      precision: 10,
      includePaths: [
        'node_modules',
      ].map((d) => path.join(__dirname, d)),
    }))
    .pipe($.postcss([
      autoprefixer({
        browsers: browsersList,
      }),
      cssnano({ zindex: false }),
    ]))
    .pipe($.rename({ suffix: '.min' }))
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest('dist/assets/css'))
    .pipe($.size({ title: 'styles' })),
);

// Concatenate And Minify JavaScript
gulp.task('scripts:components', () =>
  gulp.src(['src/components/**/*.js', 'src/objects/**/*.js', '!src/**/*/node_modules'])
    .pipe($.plumber({ errorHandler }))
    .pipe($.flatten())
    .pipe($.sourcemaps.init())
    .pipe($.babel(babelConfig))
    // Concatenate Scripts
    // .pipe($.concat('main.js'))
    // .pipe($.iife({useStrict: true}))
    // .pipe(gulp.dest('dist/assets/js'))
    // Minify Scripts
    .pipe($.uglify(uglifiyConfig))
    // .pipe($.header(banner, {pkg}))
    .pipe($.rename({ suffix: '.min' }))
    // Write Source Maps
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest('dist/assets/js'))
    .pipe($.size({ title: 'scripts components' })),
);

gulp.task('scripts:pages', () =>
  gulp.src(['src/*.js'])
    .pipe($.include({ includePaths: [
      path.join(__dirname, '/node_modules'),
      path.join(__dirname, '/src/components'),
    ] }))
    .pipe($.sourcemaps.init())
    // Concatenate Scripts
    // .pipe($.iife({useStrict: true}))
    .pipe($.babel(babelConfig))
    .pipe($.flatten())
    // .pipe(gulp.dest('dist/assets/js'))
    // Minify Scripts
    .pipe($.uglify(uglifiyConfig))
    // .pipe($.header(banner, {pkg}))
    // .pipe($.concat('main.min.js'))
    .pipe($.rename({ suffix: '.min' }))
    // Write Source Maps
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest('dist/assets/js'))
    .pipe($.size({ title: 'scripts pages' })),
);

gulp.task('modernizr', () => {
  modernizr.build(modernizrConfig, (result) =>
    $.file('modernizr.min.js', result, { src: true })
      .pipe($.uglify(uglifiyConfig))
      .pipe(gulp.dest('dist/assets/js')),
  );
});

gulp.task('copy', () =>
  mergeStream(
    gulp.src('src/fonts/**/*.{svg,ttf,woff, woff2}', { base: 'src/' }))
    .pipe(gulp.dest('dist/assets')),
);

gulp.task('lint:script', () =>
  gulp.src(['src/**/*.js'])
    .pipe($.eslint())
    .pipe($.eslint.format()),
);

gulp.task('lint:style', () =>
  gulp.src(['src/**/_*.{scss,css}'])
    .pipe($.postcss([
      stylelint(),
      reporter({ clearMessages: true }),
    ], { parser: scss })),
);

/**
 * Styleguide
 */

/**
* Site metadata for use with templates.
* @type {Object}
*/
const site = {};

function addNavData() {
  return through.obj((file, enc, cb) => {
    file.navs = {
      mainNav: 'test',
    };
    cb(null, file);
  });
}

/**
* Generates an HTML file based on a template and file metadata.
*/
function renderPage(file, options, data) {
  const tplData = data || {};
  let engine;

  if (options && options.page) {
    engine = path.extname(file.history[0]).substring(1);
  } else {
    engine = path.extname(file).substring(1);
  }

  let tpl;
  switch (engine) {
    case 'html':
      return file.contents.toString();
    case 'pug':
      if (options && options.page) {
        tpl = pug.compile(file.contents.toString(), { pretty: true });
      } else {
        tpl = pug.compileFile(file, { pretty: true });
      }
      break;
    case 'md':
    case 'markdown':
      tpl = markdown(file.contents.toString());
      return tpl;
    default:
  }

  return tpl(tplData);
}

function applyTemplate() {
  return through.obj((file, enc, cb) => {
    const data = {
      ...site,
      ...file.data,
      content: renderPage(file, { page: true }),
    };

    const isSnippet = path.relative(file.base, file.path).includes('snippets');
    let template;

    if ('template' in file.data) {
      template = file.data.template;
    } else if (isSnippet) {
      template = 'snippet.pug';
    } else {
      template = '_default.pug';
    }
    const templateFile = path.join(
      __dirname, `styleguide/_templates/${template}`);

    file.path = $.util.replaceExtension(file.path, '.html');
    // console.dir(file);
    file.contents = new Buffer(renderPage(templateFile, null, data));
    cb(null, file);
  });
}

function mainNav() {
  return through.obj((file, enc, cb) => {
    file.data.navs = {
      mainNav: 'test',
    };
    cb(null, file);
  });
}

/**
 * Generates an HTML styleguide.
 */
gulp.task('styleguide:pages', () =>
  mergeStream(
    gulp.src('styleguide/_pages/**/*.{pug,html,md,markdown}'),
    gulp.src('src/**/*.{html,md,markdown}', { base: 'src/' }),
  )
    .pipe($.frontMatter({
      property: 'data',
      remove: true,
    }))
    .pipe(mainNav())
    .pipe(applyTemplate())
    // .pipe($.data(() => ({
    //   mainNav: vinylPaths((paths) => {
    //     console.log('Paths:', paths);
    //     Promise.resolve()
    //   }),
    // })))
    // .pipe($.rename(path => {
    //   if (path.basename !== 'index') {
    //     path.dirname = path.basename;
    //     path.basename = 'index';
    //   }
    // }))
    // .pipe(mainNav())

    // .pipe($.rename(path => {
    //   if (path.basename !== 'index') {
    //     path.dirname = path.basename;
    //     path.basename = 'index';
    //   }
    // }))
    .pipe(gulp.dest('dist')),
);

/**
 * Defines the list of resources to watch for changes.
 */
function watch() {
  gulp.watch(
    [
      'styleguide/**/*.{pug,html,md,markdown}',
      'src/**/*.{html,md,markdown}',
      '!src/**/*/node_modules',
    ],
    ['styleguide:pages', reload],
  );
  gulp.watch(['src/**/*.{scss,css}', '!src/**/*/node_modules'], ['styles', reload]);
  gulp.watch(['src/**/*.js', '!src/**/*/node_modules'], ['scripts:components', 'scripts:pages', reload]);
  gulp.watch(['src/images/**/*.{svg,png,jpg,gif}'], ['images:components', 'images:svgsprite', reload]);
}

gulp.task('build', ['clean:dist'], cb => {
  runSequence(
    [
      'styleguide:pages',
      'copy',
      'styles',
      'scripts:components',
      'scripts:pages',
      'modernizr',
      'images:components',
      'images:svgsprite',
    ],
    cb);
});

gulp.task('default', cb => {
  runSequence(
    'build',
    'serve',
    cb);
});

gulp.task('lint', [
  'lint:style',
  'lint:script',
]);

/**
* Serves the landing page from "out" directory.
*/
gulp.task('serve', () => {
  browserSync({
    notify: false,
    open: false,
    server: {
      baseDir: ['dist'],
      directory: true,
    },
  });

  watch();
});

gulp.task('serve:livereload', () => {
  $.connect.server({
    root: 'dist',
    port: 5000,
    livereload: true,
  });

  watch();

  gulp.src('dist/index.html')
    .pipe($.open({ uri: 'http://localhost:5000' }));
});
