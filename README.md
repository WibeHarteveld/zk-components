# Universal component library
* [Nicolas Gallagher - Twitter Lite, React Native, and Progressive Web Apps](https://www.youtube.com/watch?v=tFFn39lLO-U)

## Styleguide documentation
* [Getting Started](styleguide/pages/docs/getting-started.md) - How to get going with the styleguide

## Monorepo
* [The bleeding-edge saga](https://medium.com/@rafaelcampos_40239/the-bleeding-edge-saga-38214faa83b)
* [The Road to Universal Components at Major League Soccer](https://labs.mlssoccer.com/the-road-to-universal-components-at-major-league-soccer-eeb7aac27e6c)
* [Up and Running with Universal Components](https://hackernoon.com/up-and-running-with-universal-components-66678132cad)

## Lerna

#### Publish package
* [Directory structure for JavaScript/Node Projects](https://gist.github.com/tracker1/59f2c13044315f88bee9)

list all packages:
`lerna ls`

List all packages that have been updated since the latest tag
`lerna ls --since`

## Yarn
* [yarn workspace](https://yarnpkg.com/en/docs/cli/workspace)

Add dependency to individual package:
`yarn workspace package_name add module_name`

add peer dependency
`yarn workspace package_name add --peer react`

## Living styleguide
* [Storybook](https://storybook.js.org/)
* [React-styleguidist](https://react-styleguidist.js.org/)

## Sketch
* [Convert Storybook into Sketch symbols](https://github.com/chrisvxd/story2sketch)

## Hosting
* [Surge](https://surge.sh/)

## Universal components

### rules

#### Don’t make it a shared component until it’s actually shared somewhere.
Too often we want to pre-optimize and future proof so we make assumptions about the future use of a component. Just because it may be isolated and an easy candidate for a shared component, doesn't mean it should be one. If there is no design or future plan tying that component to another platform then don’t add it to your universal components library!

### How to use universal components in a project
* [Getting started](https://material.io/develop/web/docs/getting-started/)

## Integration with Sketch
* Map Sketch symbols to components in React
* Adopting the naming conventions from designers

## Browser support
Browsers update very often these days, with major releases getting published every month. With each new browser version comes support for new web platform features. Thanks to open source projects such as Autoprefixer and Babel we are able to use these features while supporting older browsers. But this backward compatibility comes with a cost. We can't really keep adding prefixes,polyfills and other fallbacks to support every browser ever invented.

Browser support is based on the last 2 months of Zilveren Kruis Google Analytics stats and global usage of NL
* [Browserslist](https://github.com/browserslist/browserslist)
* [Browserslist-GA](https://github.com/browserslist/browserslist-ga)

See which browser we support: 

```bash
npx browserslist
```
