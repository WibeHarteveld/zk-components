const path = require("path");

// Export a function. Accept the base config as the only param.
module.exports = (storybookBaseConfig, env, defaultConfig) => {
  // configType has a value of 'DEVELOPMENT' or 'PRODUCTION'
  // You can change the configuration based on that.
  // 'PRODUCTION' is used when building the static version of storybook.

  // Make whatever fine-grained changes you need
  const newConfig = {
    ...defaultConfig
  };

  // Story2Sketch
  // Export bundles as libraries so we can access them on page scope.
  newConfig.output.library = "[name]";

  newConfig.module.rules.push({
    test: /\.(sass|scss)$/,
    use: [
      { loader: 'style-loader' },
      {
        loader: 'css-loader',
        options: {
          sourceMap: true,
          minimize: true,
          alias: {
            "../img": path.resolve(__dirname, "../src/base/images"),
            "../fonts": path.resolve(__dirname, "../src/base/fonts")
          },
          importLoaders: 2
        }
      },
      {
        loader: 'postcss-loader',
        options: {
          sourceMap: true
        }
      },
      {
        loader: 'sass-loader',
        options: {
          includePaths: [path.resolve(__dirname, "../node_modules")]
        }
      }
    ]
  });



  // Return the altered config
  return newConfig;
};
